package com.example.domain;
import java.util.Objects;

public class Persona {
    private Long id;
    private String nome;
    private String cognome;
    private int age;

    public Persona() {
    }

    public Persona(Long id, String nome, String cognome, int age) {
        setId(id);
        setNome(cognome);
        setCognome(cognome);
        setAge(age);
    }

    public Long getId(){
        return this.id;
    }

    public void setId(Long id){
        this.id = id;
    }

    public String getNome() {
        return this.nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCognome() {
        return this.cognome;
    }

    public void setCognome(String cognome) {
        this.cognome = cognome;
    }

    public int getAge() {
        return this.age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public int hashCode(){
        return Objects.hash(id);
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof Persona)) {
            return false;
        }
        Persona persona = (Persona) o;
        return Objects.equals(id, persona.id);
    }

    @Override
    public String toString() {
        return "{" +
            " id='" + getId() + "'" +
            " nome='" + getNome() + "'" +
            ", cognome='" + getCognome() + "'" +
            ", age='" + getAge() + "'" +
            "}";
    }
    
}
