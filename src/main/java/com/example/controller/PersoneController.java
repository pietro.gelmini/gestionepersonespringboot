package com.example.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.service.GestionePersone;
import com.example.domain.Persona;

@RestController
public class PersoneController {
    @Autowired
    GestionePersone gestionePersone;

    // @GetMapping("/{nome}")
    // public int getPersona(@PathVariable String nome){
    //     return gestionePersone.ricercaPersona(nome);
    // }

    @GetMapping("getall")
    public ResponseEntity<List<Persona>> getAll(){
        return ResponseEntity.ok().body(gestionePersone.getAll());
    }

    @PostMapping()
    public ResponseEntity<Persona> postMethodName(@RequestBody Persona entity){
        gestionePersone.addPersona(entity);
        return ResponseEntity.ok().body(entity);
    }

    @GetMapping("{id}")
    public ResponseEntity<Persona> getPersonaById(@PathVariable Long id){
        return ResponseEntity.ok().body(gestionePersone.getPersonaById(id));
    }

    @DeleteMapping("{id}")
    public ResponseEntity<Boolean> deletePersonaById(@PathVariable Long id){
        return ResponseEntity.ok().body(gestionePersone.removePersonaById(id));
    }
}
