package com.example.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;
import com.example.domain.Persona;

@Service
public class GestionePersone {
    List<Persona> persone = new ArrayList<>()
    {{
        add(new Persona(1L, "nome1", "cognome1", 10));
        add(new Persona(2L, "nome2","cognome2", 18));
        add(new Persona(3L, "nome3","cognome3", 3));
        add(new Persona(4L, "nome4","cognome4", 73));
        add(new Persona(5L, "nome5","cognome5", 51));
    }};

    public List<Persona> getAll(){
        return persone;
    }

    public Persona getPersonaById(Long id){
        for(int i = 0; i < persone.size(); i++){
            if(persone.get(i).getId().equals(id)){
                return persone.get(i);
            }
        }
        return null;
    }

    public int getPositionById(Long id){
        for(int i = 0; i < persone.size(); i++){
            if(persone.get(i).getId().equals(id)){
                return i;
            }
        }
        return -1;
    }

    public boolean removePersonaById(Long id){
        Persona personaRimossa = persone.remove(getPositionById(id));
        if(personaRimossa == null){
            return false;
        }
        return true;
    }

    public boolean addPersona(Persona persona){
        return persone.add(persona);
    }

    public int ricercaPersona(String nome){
        for(int i = 0; i < persone.size(); i++){
            if(persone.get(i).getNome().equals(nome)){
                return i;
            }
        }
        return -1;
    }
}